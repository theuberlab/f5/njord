package com.f5.AaronForster.njord.util;

import java.awt.AWTEvent;
import java.awt.Dimension;
import java.awt.event.AWTEventListener;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import javax.swing.JFrame;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.f5.AaronForster.njord.MainGuiWindow;

public class WindowSaver implements AWTEventListener {
	/**
	 * Holds the WindowSaver object
	 */
	private static WindowSaver saver;
	/**
	 * used to store a map of any/all of the windows that have been opened and/or closed.
	 */
	private Map framemap;
	//slf4j logger factory
	/**
	 * Holds our logger factory for SLF4J. This doesn't work here either.
	 */
	final Logger log = LoggerFactory.getLogger(MainGuiWindow.class);
	
	/**
	 * The default constructor.
	 */
	private WindowSaver( ) {
		framemap = new HashMap( );
	}

	/**
	 * An interface that lets us listen for window change events. This one catches a window open event which is the
	 * perfect time to tell the GUI what it's geometry should be.
	 * 
	 * @see java.awt.event.AWTEventListener#eventDispatched(java.awt.AWTEvent)
	 */
	@Override
	public void eventDispatched(AWTEvent evt) {
		try {
			if(evt.getID( ) == WindowEvent.WINDOW_OPENED) {
				ComponentEvent cev = (ComponentEvent)evt;
				if(cev.getComponent( ) instanceof JFrame) {
					JFrame frame = (JFrame)cev.getComponent( );
					loadSettings(frame);
				}
			}
		} catch(Exception ex) {
			System.out.println("Error: " + ex.toString( ));
		}
	}

	/**
	 * Loads geometry settings from the properties file for the specified frame.
	 * 
	 * @param frame
	 * @throws IOException
	 */
	public static void loadSettings(JFrame frame) throws IOException {
		String userHome = System.getProperty("user.home");
		
		//http://www.particle.kth.se/~lindsey/JavaCourse/Book/Part1/Java/Chapter10/Preferences.html
		//TODO: Replace this whole file thing with java.util.prefs maybe?
		//Check and see if the prefs and bigips directory exists and create it if it doesn't
		String settingsDirPath = userHome + "/.f5/njord/";
		File settingsDirectory = new File(settingsDirPath);
		if ( !settingsDirectory.exists() ) {
			System.out.println("Preferences directory doesn't exist!");
			// Create multiple directories
		}
		
		File geometryFile = new File(settingsDirectory + "/geometry.properties");

		if ( !geometryFile.exists() ) {
			System.out.println("Geometry file doesn't exist");
		} else {
			System.out.println("Geometry file exists");
			// Get the properties from a file
			Properties settings = new Properties( );
			settings.load(new FileInputStream(geometryFile));
			String name = frame.getName( );
			int x = getInt(settings,name+".x",100);
			int y = getInt(settings,name+".y",100);
			int w = getInt(settings,name+".w",500);
			int h = getInt(settings,name+".h",500);
//			int extendedState = getInt(settings,name+".extendedState",JFrame.MAXIMIZED_BOTH);
			int extendedState = getInt(settings,name+".extendedState",0);
			frame.setLocation(x,y);
			frame.setSize(new Dimension(w,h));
			frame.setExtendedState(extendedState);
			saver.framemap.put(name,frame);
			frame.validate( );
		}
	}

	/**
	 * Writes window geometry to the properties file.
	 * @throws IOException
	 */
	public static void saveSettings( ) throws IOException {
		String userHome = System.getProperty("user.home");
		
		String settingsDirPath = userHome + "/.f5/njord/";
		File settingsDirectory = new File(settingsDirPath);
		boolean success = false;
		if ( !settingsDirectory.exists() ) {
		success = (new File(settingsDirPath)).mkdirs();
		  if (success) {
			  System.out.println("Directories: " + settingsDirPath + " created");
		  }
		}
		
		File geometryFile = new File(settingsDirectory + "/geometry.properties");
		
		if ( !geometryFile.exists() ) {
			System.out.println("Geometry file doesn't exist");
			try {
				success = geometryFile.createNewFile();
			    if (success) {
			    	System.out.println( geometryFile + " created");
				}
			} catch (IOException e) {
				f5ExceptionHandler exceptionHandler = new f5ExceptionHandler(e);
				exceptionHandler.processException();
			}
		} else {
			System.out.println("Gometry file exists");
		}
		Properties settings = new Properties( );
		settings.load(new FileInputStream(geometryFile));
		
		Iterator it = saver.framemap.keySet( ).iterator( );
		while(it.hasNext( )) {    
			String name = (String)it.next( ); 
			JFrame frame = (JFrame)saver.framemap.get(name);
			// If the frame is maximized let's save only that. If not let's save the actual dimensions.
			if (frame.getExtendedState( ) == JFrame.MAXIMIZED_BOTH ) {
				settings.setProperty(name+".extendedState",""+frame.getExtendedState( ));
			} else {
				settings.setProperty(name+".x",""+frame.getX( ));    
				settings.setProperty(name+".y",""+frame.getY( ));    
				settings.setProperty(name+".w",""+frame.getWidth( ));    
				settings.setProperty(name+".h",""+frame.getHeight( ));	
				settings.setProperty(name+".extendedState",""+frame.getExtendedState( ));
			}
		} 
		settings.store(new FileOutputStream(geometryFile),null); 
	}
	
//	public static void putGeometry(String name, JFrame frame, )
	/**
	 * Settings are all saved as string. unfortunately we're dealing with settings that all need to be represented as ints.
	 * This method retrieves the string of the int from our preferences map and returns it as an it. If not found it returns the 
	 * optionally supplied int provided.
	 * 
	 * @param props An instance of preferences.
	 * @param name The name of the property to retrieve from the preferences object.
	 * @param value The default value to return if the property isn't found.
	 * @return
	 */
	public static int getInt(Properties props, String name, int value) { 
		String v = props.getProperty(name); 
		if(v == null) {
			return value;
		}
		return Integer.parseInt(v);

	}

	public static WindowSaver getInstance( ) {
		if(saver == null) {
			saver = new WindowSaver( );
		}
		return saver;
	}
}