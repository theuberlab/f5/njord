/**
 * 
 */
package com.f5.AaronForster.njord.ui;

import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.filechooser.FileSystemView;

import org.fife.ui.autocomplete.AutoCompletion;
import org.fife.ui.autocomplete.BasicCompletion;
import org.fife.ui.rsyntaxtextarea.AbstractTokenMakerFactory;
import org.fife.ui.rsyntaxtextarea.FileLocation;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rsyntaxtextarea.SyntaxScheme;
import org.fife.ui.rsyntaxtextarea.TextEditorPane;
import org.fife.ui.rsyntaxtextarea.Token;
import org.fife.ui.rsyntaxtextarea.TokenMakerFactory;
import org.slf4j.Logger;

import com.f5.AaronForster.njord.MainGuiWindow;
import com.f5.AaronForster.njord.util.NjordCompletionProvider;
import com.f5.AaronForster.njord.util.NjordConstants;
import com.f5.AaronForster.njord.util.NjordFileLocation;
import com.f5.AaronForster.njord.util.NjordParser;
import com.f5.AaronForster.njord.util.f5ExceptionHandler;
/*
 * ########################################################################3
 *  NOT CURRENTLY IMPLEMENTED
 *  #########################################################################
 *  Eventually I want to be able to use this class. It should extend JTabbedPane and basically just ALWAYS create a pane with four tabs
 *  IE Implementation, Presentation, help and "other"
 *  Or do I want three tabs and a headers section?
 *  
 */
/**
 * @author forster
 *
 */
public class NjordiAppsTemplatePane extends JTabbedPane {
	public String nl = System.getProperty("line.separator");
	public String objectName = null;
	public iControl.Interfaces ic = null;
	public Object anObject = null; //This is totally hackish
	public boolean exists = false; // Used to determine if the file needs to be created. IE is this a new file we've created or one we have loaded.
	public boolean local = false;
	public MainGuiWindow owner = null;
	public File fileHandle = null;
	public int objectType = 0;
	
	public ImageIcon icon = NjordTreeRenderer.createImageIcon("/resources/iRuleIconClean.png");
	public String implementationPortionText = "";
	public String presentationPortionText = "";
	public String htmlHelpPortionText = "";
	
	public TextEditorPane implementationPortion = null;
	public TextEditorPane presentationPortion = null;
	public TextEditorPane htmlHelpPortion = null;
	
	private String logPrefix = "NjordiAppsTemplatePane";
	private Logger log;
	
	/**
	 * 
	 */
	public NjordiAppsTemplatePane() {
//		JTabbedPane iAppsTemplateEditorPane = new JTabbedPane();

//		JComponent panel1 = makeTemplatePanel("Implementation");
		this.addTab("Tab 1", icon, implementationPortion,
		                  "Does nothing");
		this.setMnemonicAt(0, KeyEvent.VK_1);

//		JComponent panel2 = makeTemplatePanel("Presentation");
		this.addTab("Tab 2", icon, presentationPortion,
		                  "Does twice as much nothing");
		this.setMnemonicAt(1, KeyEvent.VK_2);

//		JComponent panel3 = makeTemplatePanel("HTML Help");
		this.addTab("Tab 3", icon, htmlHelpPortion,
		                  "Still does nothing");
		this.setMnemonicAt(2, KeyEvent.VK_3);

		JComponent panel4 = makeTemplatePanel("Everything Else");
		this.addTab("Tab 4", icon, panel4,
		                  "Really Does Nothing");
		this.setMnemonicAt(2, KeyEvent.VK_4);
	}
	
	public NjordiAppsTemplatePane(MainGuiWindow owner, String templatePaneName, iControl.Interfaces ic) {
		this.owner = owner;
		this.objectName = templatePaneName;
		this.ic = ic;
		this.log = owner.log;
		
		setImplementation();
		setPresentation();
		setHTMLHelp();
		
//		JTabbedPane iAppsTemplateEditorPane = new JTabbedPane();

//		JComponent panel1 = makeTemplatePanel("Implementation");
//		JPanel implementationPanel = new JPanel();
//		implementationPanel.add(implementationPortion);
		this.addTab("Implementaion", icon, implementationPortion,
		                  "Does nothing");
		this.setMnemonicAt(0, KeyEvent.VK_1);

//		JComponent panel2 = makeTemplatePanel("Presentation");
		this.addTab("Presentation", icon, presentationPortion,
		                  "Does twice as much nothing");
		this.setMnemonicAt(1, KeyEvent.VK_2);

//		JComponent panel3 = makeTemplatePanel("HTML Help");
		this.addTab("HTML Help", icon, htmlHelpPortion,
		                  "Still does nothing");
		this.setMnemonicAt(2, KeyEvent.VK_3);

		JComponent panel4 = makeTemplatePanel("Everything Else");
		this.addTab("Everything Else", icon, panel4,
		                  "Really Does Nothing");
		this.setMnemonicAt(2, KeyEvent.VK_4);
		
		
	}
	public boolean isLocal() {
		return false;
	}
	
	public boolean isDirty() {
		return false;
	}
	
	public void setImplementation(String implementationText) {
    	NjordFileLocation fileLocation = new NjordFileLocation(owner, objectName, NjordConstants.DEFINITION_PORTION_IMPLEMENTATION, ic); 
    	implementationPortion = getImplementationEditor(fileLocation, objectName);
	}
	public void setPresentation(String presentationText) {
    	NjordFileLocation fileLocation = new NjordFileLocation(owner, objectName, NjordConstants.DEFINITION_PORTION_PRESENTATION, ic); 
    	presentationPortion = getPresentationEditor(fileLocation, objectName);
	}
	public void setHTMLHelp(String htmlHelpText) {
    	NjordFileLocation fileLocation = new NjordFileLocation(owner, objectName, NjordConstants.DEFINITION_PORTION_HTML_HELP, ic); 
    	htmlHelpPortion = getHTMLHelpEditor(fileLocation, objectName);
	}
	
	public void setImplementation() {
    	NjordFileLocation fileLocation = new NjordFileLocation(owner, objectName, NjordConstants.DEFINITION_PORTION_IMPLEMENTATION, ic); 
    	implementationPortion = getImplementationEditor(fileLocation, objectName);
	}
	public void setPresentation() {
    	NjordFileLocation fileLocation = new NjordFileLocation(owner, objectName, NjordConstants.DEFINITION_PORTION_PRESENTATION, ic); 
    	presentationPortion = getPresentationEditor(fileLocation, objectName);
	}
	public void setHTMLHelp() {
    	NjordFileLocation fileLocation = new NjordFileLocation(owner, objectName, NjordConstants.DEFINITION_PORTION_HTML_HELP, ic); 
    	htmlHelpPortion = getHTMLHelpEditor(fileLocation, objectName);
	}
	
	public TextEditorPane getImplementationEditor(NjordFileLocation definitionLocation, String templateName) {
		AbstractTokenMakerFactory atmf = (AbstractTokenMakerFactory) TokenMakerFactory.getDefaultInstance();
		atmf.putMapping("SYNTAX_STYLE_TCL", "com.f5.AaronForster.njord.util.iRulesTokenMaker");
		TokenMakerFactory.setDefaultInstance(atmf); //Don't know if I need this line or not
		
		TextEditorPane editor = null;
		try {
    		editor = new TextEditorPane(0, true, definitionLocation, "ISO-8859-1");
    	} catch (IOException e) {
    		f5ExceptionHandler exceptionHandler = new f5ExceptionHandler(e);
    		exceptionHandler.processException();
    	}
		
    	editor.setName(templateName);
    	editor.discardAllEdits(); //This prevents undo from 'undoing' the loading of the editor with an iRule. Should be unnessesary now that I'm using FileLocation.
    	// I need to add NjordDocumentListener types for these
//    	editor.getDocument().addDocumentListener(new NjordDocumentListener(this));
    	
    	//Syntax Highlighting items
    	// ADD one for TMSH
    	editor.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_TCL);
    	editor.setCodeFoldingEnabled(true);
    	editor.setAntiAliasingEnabled(true);
    	
    	SyntaxScheme scheme = editor.getSyntaxScheme();
    	
    	log.debug(logPrefix + "Vars currently have " + scheme.getStyle(Token.VARIABLE).foreground);
    	
    	scheme.getStyle(Token.ANNOTATION).foreground = owner.annotationColor;
    	scheme.getStyle(Token.VARIABLE).foreground = owner.variableColor;
//    	scheme.getStyle(Token.IDENTIFIER).foreground = identifyerColor; //I think 'identifyer' is all the other words.
    	scheme.getStyle(Token.FUNCTION).foreground = owner.functionColor;
    	scheme.getStyle(Token.REGEX).foreground = owner.regexColor;
    	scheme.getStyle(Token.COMMENT_EOL).foreground = owner.commentColor;
//    	scheme.getStyle(Token.COMMENT_MULTILINE).foreground = commentColor; // I don't think there's a multi-line comment defined yet
    	scheme.getStyle(Token.RESERVED_WORD_2).foreground = owner.reservedWord2Color;
    	scheme.getStyle(Token.RESERVED_WORD).foreground = owner.reservedWordColor;
    	scheme.getStyle(Token.OPERATOR).foreground = owner.operatorColor;
//    	scheme.getStyle(Token.LITERAL_STRING_DOUBLE_QUOTE).foreground = doublequoteColor;
    	scheme.getStyle(Token.LITERAL_BACKQUOTE).foreground = owner.backquoteColor;
    	scheme.getStyle(Token.SEPARATOR).foreground = owner.bracketColor;
    	
    	// A CompletionProvider is what knows of all possible completions, and
        // analyzes the contents of the text area at the caret position to
        // determine what completion choices should be presented. Most
        // instances of CompletionProvider (such as DefaultCompletionProvider)
        // are designed so that they can be shared among multiple text
        // components.
        NjordCompletionProvider provider = createCompletionProvider(NjordConstants.DEFINITION_PORTION_IMPLEMENTATION);
        provider.setAutoActivationRules(true, ":_");
        
        // An AutoCompletion acts as a "middle-man" between a text component
        // and a CompletionProvider. It manages any options associated with
        // the auto-completion (the popup trigger key, whether to display a
        // documentation window along with completion choices, etc.). Unlike
        // CompletionProviders, instances of AutoCompletion cannot be shared
        // among multiple text components.
        AutoCompletion ac = new AutoCompletion(provider);
        ac.setAutoActivationDelay(0);
        ac.setAutoActivationEnabled(true);
        ac.install(editor);
        
        //Gotta add a parser here I think to catch the words and make them links?
        NjordParser parser = new NjordParser();
        editor.addParser(parser);
        
        //Let's see if setting this true or false fixes the odd word wrap== new line thing
        editor.setWrapStyleWord(true);
        
        JFileChooser fc = new JFileChooser();  
		FileSystemView fv = fc.getFileSystemView();  
		File documentsDir = fv.getDefaultDirectory();
        String templatesDirPath = documentsDir.getAbsolutePath() + "/Njord/Templates/";
        editor.setTemplateDirectory(templatesDirPath);
        editor.setTemplatesEnabled(true);
       
        //Some stuff to add to the editor
        // Try setEncoding
        //setTemplateDirectory(dir)
        //setTemplatesEnabled(enabled)
        //setIconGroup(group)
        //setMarkOccurrences(markOccurrences) gotta update my search to include that
        //This needs to be setable by a preference
        //setEOLMarkersVisible(boolean visible)
        //setPaintTabLines(paint)
        //.setWhitespaceVisible(visible)
        
//    	ruleEditor.discardAllEdits(); //This prevents undo from 'undoing' the loading of the editor with an iRule. Should be unnessesary now that I'm using FileLocation.
        return editor;
	}

	public TextEditorPane getPresentationEditor(FileLocation definitionLocation, String templateName) {
		TextEditorPane editor = null;
		try {
    		editor = new TextEditorPane(0, true, definitionLocation, "ISO-8859-1");
    	} catch (IOException e) {
    		f5ExceptionHandler exceptionHandler = new f5ExceptionHandler(e);
    		exceptionHandler.processException();
    	}
		
    	editor.setName(templateName);
    	editor.discardAllEdits(); //This prevents undo from 'undoing' the loading of the editor with an iRule. Should be unnessesary now that I'm using FileLocation.
    	// I need to add NjordDocumentListener types for these
//    	editor.getDocument().addDocumentListener(new NjordDocumentListener(this));
    	
    	//Syntax Highlighting items
    	// ADD one for TMSH
    	editor.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_TCL);
    	// This will become like the below once I write the APL TokenMaker
//    	editor.setSyntaxEditingStyle("SYNTAX_STYLE_APL");
    	editor.setCodeFoldingEnabled(true);
    	editor.setAntiAliasingEnabled(true);
    	
    	SyntaxScheme scheme = editor.getSyntaxScheme();
    	
    	log.debug(logPrefix + "Vars currently have " + scheme.getStyle(Token.VARIABLE).foreground);
    	
    	scheme.getStyle(Token.ANNOTATION).foreground = owner.annotationColor;
    	scheme.getStyle(Token.VARIABLE).foreground = owner.variableColor;
//    	scheme.getStyle(Token.IDENTIFIER).foreground = identifyerColor; //I think 'identifyer' is all the other words.
    	scheme.getStyle(Token.FUNCTION).foreground = owner.functionColor;
    	scheme.getStyle(Token.REGEX).foreground = owner.regexColor;
    	scheme.getStyle(Token.COMMENT_EOL).foreground = owner.commentColor;
//    	scheme.getStyle(Token.COMMENT_MULTILINE).foreground = commentColor; // I don't think there's a multi-line comment defined yet
    	scheme.getStyle(Token.RESERVED_WORD_2).foreground = owner.reservedWord2Color;
    	scheme.getStyle(Token.RESERVED_WORD).foreground = owner.reservedWordColor;
    	scheme.getStyle(Token.OPERATOR).foreground = owner.operatorColor;
//    	scheme.getStyle(Token.LITERAL_STRING_DOUBLE_QUOTE).foreground = doublequoteColor;
    	scheme.getStyle(Token.LITERAL_BACKQUOTE).foreground = owner.backquoteColor;
    	scheme.getStyle(Token.SEPARATOR).foreground = owner.bracketColor;
    	
    	// A CompletionProvider is what knows of all possible completions, and
        // analyzes the contents of the text area at the caret position to
        // determine what completion choices should be presented. Most
        // instances of CompletionProvider (such as DefaultCompletionProvider)
        // are designed so that they can be shared among multiple text
        // components.
        NjordCompletionProvider provider = createCompletionProvider(NjordConstants.DEFINITION_PORTION_IMPLEMENTATION);
        provider.setAutoActivationRules(true, ":_");
        
        // An AutoCompletion acts as a "middle-man" between a text component
        // and a CompletionProvider. It manages any options associated with
        // the auto-completion (the popup trigger key, whether to display a
        // documentation window along with completion choices, etc.). Unlike
        // CompletionProviders, instances of AutoCompletion cannot be shared
        // among multiple text components.
        AutoCompletion ac = new AutoCompletion(provider);
        ac.setAutoActivationDelay(0);
        ac.setAutoActivationEnabled(true);
        ac.install(editor);
        
        //Gotta add a parser here I think to catch the words and make them links?
        NjordParser parser = new NjordParser();
        editor.addParser(parser);
        
        //Let's see if setting this true or false fixes the odd word wrap== new line thing
        editor.setWrapStyleWord(true);
        
        JFileChooser fc = new JFileChooser();  
		FileSystemView fv = fc.getFileSystemView();  
		File documentsDir = fv.getDefaultDirectory();
        String templatesDirPath = documentsDir.getAbsolutePath() + "/Njord/Templates/";
        editor.setTemplateDirectory(templatesDirPath);
        editor.setTemplatesEnabled(true);
       
        //Some stuff to add to the editor
        // Try setEncoding
        //setTemplateDirectory(dir)
        //setTemplatesEnabled(enabled)
        //setIconGroup(group)
        //setMarkOccurrences(markOccurrences) gotta update my search to include that
        //This needs to be setable by a preference
        //setEOLMarkersVisible(boolean visible)
        //setPaintTabLines(paint)
        //.setWhitespaceVisible(visible)
        
//    	ruleEditor.discardAllEdits(); //This prevents undo from 'undoing' the loading of the editor with an iRule. Should be unnessesary now that I'm using FileLocation.
        return editor;
	}
	
	public TextEditorPane getHTMLHelpEditor(FileLocation definitionLocation, String templateName) {
		TextEditorPane editor = null;
		try {
    		editor = new TextEditorPane(0, true, definitionLocation, "ISO-8859-1");
    	} catch (IOException e) {
    		f5ExceptionHandler exceptionHandler = new f5ExceptionHandler(e);
    		exceptionHandler.processException();
    	}
		
    	editor.setName(templateName);
    	editor.discardAllEdits(); //This prevents undo from 'undoing' the loading of the editor with an iRule. Should be unnessesary now that I'm using FileLocation.
    	// I need to add NjordDocumentListener types for these
//    	editor.getDocument().addDocumentListener(new NjordDocumentListener(this));
    	
    	//Syntax Highlighting items
    	// ADD one for TMSH
    	editor.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_HTML);
    	editor.setCodeFoldingEnabled(true);
    	editor.setAntiAliasingEnabled(true);
    	
    	SyntaxScheme scheme = editor.getSyntaxScheme();
    	
    	log.debug(logPrefix + "Vars currently have " + scheme.getStyle(Token.VARIABLE).foreground);
    	
    	scheme.getStyle(Token.ANNOTATION).foreground = owner.annotationColor;
    	scheme.getStyle(Token.VARIABLE).foreground = owner.variableColor;
//    	scheme.getStyle(Token.IDENTIFIER).foreground = identifyerColor; //I think 'identifyer' is all the other words.
    	scheme.getStyle(Token.FUNCTION).foreground = owner.functionColor;
    	scheme.getStyle(Token.REGEX).foreground = owner.regexColor;
    	scheme.getStyle(Token.COMMENT_EOL).foreground = owner.commentColor;
//    	scheme.getStyle(Token.COMMENT_MULTILINE).foreground = commentColor; // I don't think there's a multi-line comment defined yet
    	scheme.getStyle(Token.RESERVED_WORD_2).foreground = owner.reservedWord2Color;
    	scheme.getStyle(Token.RESERVED_WORD).foreground = owner.reservedWordColor;
    	scheme.getStyle(Token.OPERATOR).foreground = owner.operatorColor;
//    	scheme.getStyle(Token.LITERAL_STRING_DOUBLE_QUOTE).foreground = doublequoteColor;
    	scheme.getStyle(Token.LITERAL_BACKQUOTE).foreground = owner.backquoteColor;
    	scheme.getStyle(Token.SEPARATOR).foreground = owner.bracketColor;
    	
    	// A CompletionProvider is what knows of all possible completions, and
        // analyzes the contents of the text area at the caret position to
        // determine what completion choices should be presented. Most
        // instances of CompletionProvider (such as DefaultCompletionProvider)
        // are designed so that they can be shared among multiple text
        // components.
        NjordCompletionProvider provider = createCompletionProvider(NjordConstants.DEFINITION_PORTION_IMPLEMENTATION);
        provider.setAutoActivationRules(true, ":_");
        
        // An AutoCompletion acts as a "middle-man" between a text component
        // and a CompletionProvider. It manages any options associated with
        // the auto-completion (the popup trigger key, whether to display a
        // documentation window along with completion choices, etc.). Unlike
        // CompletionProviders, instances of AutoCompletion cannot be shared
        // among multiple text components.
        AutoCompletion ac = new AutoCompletion(provider);
        ac.setAutoActivationDelay(0);
        ac.setAutoActivationEnabled(true);
        ac.install(editor);
        
        //Gotta add a parser here I think to catch the words and make them links?
        NjordParser parser = new NjordParser();
        editor.addParser(parser);
        
        //Let's see if setting this true or false fixes the odd word wrap== new line thing
        editor.setWrapStyleWord(true);
        
        JFileChooser fc = new JFileChooser();  
		FileSystemView fv = fc.getFileSystemView();  
		File documentsDir = fv.getDefaultDirectory();
        String templatesDirPath = documentsDir.getAbsolutePath() + "/Njord/Templates/";
        editor.setTemplateDirectory(templatesDirPath);
        editor.setTemplatesEnabled(true);
       
        //Some stuff to add to the editor
        // Try setEncoding
        //setTemplateDirectory(dir)
        //setTemplatesEnabled(enabled)
        //setIconGroup(group)
        //setMarkOccurrences(markOccurrences) gotta update my search to include that
        //This needs to be setable by a preference
        //setEOLMarkersVisible(boolean visible)
        //setPaintTabLines(paint)
        //.setWhitespaceVisible(visible)
        
//    	ruleEditor.discardAllEdits(); //This prevents undo from 'undoing' the loading of the editor with an iRule. Should be unnessesary now that I'm using FileLocation.
        return editor;
	}
	
	
	
	
	/**
	 * Create a simple provider that adds some Java-related completions.
	 * 
	 * @return The completion provider.
	 */
	private NjordCompletionProvider createCompletionProvider(int templateDefinitionPortion) {
		
		switch (templateDefinitionPortion) {
			case NjordConstants.DEFINITION_PORTION_IMPLEMENTATION: {
				String tclCommandsPath = "/resources/tclCommandsUncategorized.txt"; // Built in tcl commands
				break;
			}
			case NjordConstants.DEFINITION_PORTION_PRESENTATION: {
				
				break;
			}
			case NjordConstants.DEFINITION_PORTION_HTML_HELP: {
				
				break;
			}
			case NjordConstants.DEFINITION_PORTION_OTHER: {
				
				break;
			}
		}
		// A DefaultCompletionProvider is the simplest concrete implementation
		// of CompletionProvider. This provider has no understanding of
		// language semantics. It simply checks the text entered up to the
		// caret position for a match against known completions. This is all
		// that is needed in the majority of cases.
		NjordCompletionProvider provider = new NjordCompletionProvider();

		String eventsPath = "/resources/iRulesEventsUncategorized.txt"; // CLIENT_ACCEPTED, CACHE_REQUEST, etc.
		String operatorsPath = "/resources/iRulesOperatorsUncategorized.txt";
		String statementsPath = "/resources/iRulesStatementsUncategorized.txt"; // drop, pool and more
		String functionsPath = "/resources/iRulesFunctionsUncategorized.txt"; // findstr, class and others
		String commandsPath = "/resources/iRulesCommandsUncategorized.txt"; // HTTP::return etc etc
		String tclCommandsPath = "/resources/tclCommandsUncategorized.txt"; // Built in tcl commands
		
	    BufferedReader reader = null;
	    
	    // Now go through the above list in order and add the contents of each file to the completion provider.
	    List<String> keyWordsLists = new ArrayList<String>(Arrays.asList(eventsPath, operatorsPath, statementsPath, functionsPath,
	    		commandsPath, tclCommandsPath));
	    
	    for (String keyWordList : keyWordsLists) {
	    	boolean append = false;
	    	if (keyWordList.matches("/resources/iRulesEventsUncategorized.txt")) {
	    		append = true;
	    	}
	    	try {
				reader = new BufferedReader(new InputStreamReader(MainGuiWindow.class.getResourceAsStream(keyWordList)));
				String str;
				while ((str = reader.readLine()) != null) {
					if (append == true) { // If this is the events list then let's append a space and an open curly brace.
						str = str + " {";
					}
					provider.addCompletion(new BasicCompletion(provider, str));
				}
			} catch (IOException e) {
				f5ExceptionHandler exceptionHandler = new f5ExceptionHandler(e, owner, log);
				exceptionHandler.processException();
			} finally {
				try {
					if (reader !=null) { 
						reader.close();  
					}
				} catch (IOException e) {
					f5ExceptionHandler exceptionHandler = new f5ExceptionHandler(e);
					exceptionHandler.processException();
				}
			}
	    }
		return provider;
	}
	
	/**
	 * @param arg0
	 */
	public NjordiAppsTemplatePane(int arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public NjordiAppsTemplatePane(int arg0, int arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}
	
    public JComponent makeTemplatePanel(String text) {
        JPanel panel = new JPanel(false);
        JLabel filler = new JLabel(text);
        filler.setHorizontalAlignment(JLabel.CENTER);
        panel.setLayout(new GridLayout(1, 1));
        panel.add(filler);
        return panel;
    }
    
}
